<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="mid">
 */


if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

do_action('apollo13framework_before_html');

?><!DOCTYPE html>
<!--[if IE 9]>    <html class="no-js lt-ie10" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
<?php do_action('apollo13framework_head_start'); ?>

<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width,initial-scale=1" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="profile" href="https://gmpg.org/xfn/11" />
<?php
    wp_head();
?>
</head>

<body id="top" <?php body_class(); apollo13framework_schema_args('body'); ?>>

<?php if ( is_front_page() ):  ?>
    <style>
        /*todo минифицировать */
        .main-page-preloader{
            position: fixed;
            z-index: -101;
            width: 100%;
            height: 100%;
            background: black;
        }

        .main-page-preloader__erase {
            animation: erase-animation 2.5s linear;
        }

        @keyframes erase-animation {
            0% {
                z-index: 101;
                opacity: 1;
            }
            90% {
                z-index: 101;
                opacity: 1;
            }
            100% {
                z-index: 101;
                opacity: 0;
            }
        }

        .main-page-preloader_white-layer {
            position: fixed;
            z-index: 102;
            height: 100%;
            width: 100%;
            background: white;
            transition: 2s all ease-in-out;
            -webkit-transition: 2s all ease-in-out;
        }

        .main-page-preloader_white-layer__close {
            animation: white-layer__close-animation 2s ease-in-out;
            animation-fill-mode: forwards;
        }

        @keyframes white-layer__close-animation {
            0% {
                height: 100%;
            }
            100% {
                height: 0%;
            }
        }

        .main-page-preloader_logo{
            position: fixed;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            font-weight: 900;
            font-size: 3em;
            color: white;
            z-index: 103;
            text-align: center;
        }

        .main-page-preloader_logo__change-color {
            animation: change-color-animation 2s linear;
            /*animation-fill-mode: forwards;*/
        }

        @keyframes change-color-animation {
            0% {
                color: black;
            }
            50% {
                color: black;
            }
            60% {
                color: white;
            }
            100% {
                color: white;
            }
        }

        .transition-layout{
            position: fixed;
            z-index: 105;
            left: 0;
            /*background: #31363a;*/
            background: #1f2224;
            width: 100%;
        }

        .transition-layout__close {
            animation: close-animation 2s;
            animation-fill-mode: forwards;
        }

        @keyframes close-animation {
            0%{
                bottom: 0;
                height: 0;
            }
            100%{
                bottom: 0;
                height: 100%;
            }
        }

    </style>

    <div class="main-page-preloader main-page-preloader__erase">
        <div class="main-page-preloader_white-layer main-page-preloader_white-layer__close"></div>
        <span class="main-page-preloader_logo main-page-preloader_logo__change-color">ART MASSIV</span>
    </div>

    <div class="transition-layout"></div>
<?php else:  ?>
    <style>
        .transition-layout{
            position: fixed;
            z-index: 105;
            left: 0;
            /*background: #31363a;*/
            background: #1f2224;
            width: 100%;
        }

        .transition-layout__open {
            animation: open-animation 2s;
            animation-fill-mode: forwards;
        }

        @keyframes open-animation {
            0%{
                height: 100%;
            }
            100%{
                height: 0;
            }
        }

        .transition-layout__close {
            animation: close-animation 2s;
            animation-fill-mode: forwards;
        }

        @keyframes close-animation {
            0%{
                bottom: 0;
                height: 0;
            }
            100%{
                bottom: 0;
                height: 100%;
            }
        }
    </style>

    <div class="transition-layout transition-layout__open"></div>
<?php endif; ?>


<?php
// WordPress 5.2 support
if ( function_exists( 'wp_body_open' ) ) {
    wp_body_open();
}
//WordPress < 5.2
else {
    do_action( 'wp_body_open' );
}
do_action('apollo13framework_body_start');
?>
<div class="whole-layout">
<?php
    apollo13framework_page_preloader();
    apollo13framework_page_background();
    if( ! apply_filters('apollo13framework_only_content', false) ) {
        apollo13framework_theme_header();
    }
    ?>
    <div id="mid" class="to-move <?php echo esc_attr( apollo13framework_get_mid_classes() ); ?>">