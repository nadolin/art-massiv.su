//custom.js for my custom scripts

jQuery(document).ready(function($){

  function is_shown(target) {
    let wt = $(window).scrollTop();
    let wh = $(window).height();
    let eh = $(target).outerHeight();
    let et = $(target).offset().top;

    if (wt + wh >= et && wt + wh - eh * 2 <= et + (wh - eh)){
      return true;
    } else {
      return false;
    }
  }

  window.addEventListener( 'load', function() {
    let scales = document.querySelectorAll('.scale img'),
      docHeight = document.documentElement.offsetHeight;
//todo yiucompressor не обрабатывает for ( let scale in scales) {}
    for (let i = 0; i < scales.length; i++) {
      window.addEventListener('scroll', function () {
        // normalize scroll position as percentage
        if (is_shown(scales[i])) {
          let scrolled = 1 +  window.scrollY / (docHeight - window.innerHeight) / 4,
            transformValue = 'scale(' + scrolled + ')';

          scales[i].style.WebkitTransform = transformValue;
          scales[i].style.MozTransform = transformValue;
          scales[i].style.OTransform = transformValue;
          scales[i].style.transform = transformValue;
        }
      }, false);
    }
  }, false);


  let images = document.querySelectorAll('.paralax img');
  new simpleParallax(images, {
    scale: 1.1
  });

//Растягиваем stretch-секцию по высоте на 100vh, без учета адресной строки на мобилках
//Fit to screen\По размеру экрана работает некорректно
  let stretched = document.querySelector('.elementor-section-stretched .elementor-container.elementor-column-gap-default');
  if (stretched) stretched.style.height = window.innerHeight + 'px';

// custom preloader
  $("#menu-main-menu a").click(trueLinkClick);
  $(".logo-container a").click(trueLinkClick);
  $("#footer a:not([target='_blank'])").click(trueLinkClick);
  $(".works-grid-container a").click(trueLinkClick);
  $(".cpt-nav a").click(trueLinkClick);
  $(".cpt-categories a").click(trueLinkClick);
  $(".similar-works-frame a").click(trueLinkClick);

  $("div[data-column-clickable]").click(function (event) {
    let linkLocation = window.location.protocol + '//' + window.location.host + this.dataset.columnClickable;

    event.preventDefault();
    $('.transition-layout').addClass('transition-layout__close');
    $('body').css('overflow', 'hidden');
    setTimeout(function () {
      window.location = linkLocation;
    }, 2000);
  });

  function trueLinkClick(event) {
    let linkLocation = this.href;

    event.preventDefault();
    $('.transition-layout').addClass('transition-layout__close');
    $('body').css('overflow', 'hidden');
    setTimeout(function () {
      window.location = linkLocation;
    }, 2000);
  };

});